﻿using System;
using System.Collections.Generic;

namespace Zhouli.DbEntity.Models
{
    public partial class DictAuthorityType
    {
        public int AuthorityTypeId { get; set; }
        public string AuthorityTypeName { get; set; }
    }
}
